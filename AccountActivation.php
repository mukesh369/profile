<?php

// This page handles the request after clicking on the account activation link in the mail.

session_start() ;
require "Utilities/Validation.php";
if (isset($_GET["id"])) {
    require "Config/Database.php";
    require "Objects/Employee.php";
    require "Utilities/Security.php";
    

    $security = new Security();
    
    $id = $security->hash('decrypt', $_GET["id"]);

    $database = new Database();
    $db = $database->getConnection();


    $emp = new Employee($db);

    $update = $emp->activateAccount($id);

    /* 0 - Account already activated
    // 1 - Account Activation successfull.
    // 2 - No account exists with the given id.
    */
    switch ($update) {
        case 0:
            Validation::display("This account is already activated ");
            break;
        case 1:
            Validation::display("Congratulations. Your account has been successfully activated");
            break;
        case 2:
            Validation::display("No Account exists with the given id .");
            break;
        default:
            Validation::display($update);
    }
    
    /* After account activation if session is set go to profile page else go to login page. */
    if (isset($_SESSION["userid"])) {
        header("refresh:4;url=Profile.php");
    } else {
        header("refresh:4;url=index.php");
    }
} else {
     Validation::display("Forbidden access.");
}
