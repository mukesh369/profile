<body id='signUpPage'>
  <h2 class="" ><center>Sign Up</center></h2>
  <div id="id01" class="container-fluid">
    <!-- Icon -->  
    <?php require "UserIcon.php" ; ?>
    <!-- Sign up form -->
    <form class="form-horizontal" id='signUpForm' method="POST" action="SignUpUser.php" onsubmit="return validateForm()">
      <fieldset>
      <div class="alert" id="message"></div>
      <input type="hidden" name="csrf" value="<?php echo $_SESSION['csrf_token_signUp']; ?>">
      <div class="form-group">
        <!-- Name with prefix dropdown , firstName ,middleName, lastName inputs -->
        <label class="control-label col-sm-2" for="name">Name: <span class='inputInfo'>*</span></label>
        <div class="col-sm-10">
          <div class="row">
            <div class="col-sm-3">
              <select class="form-control" name="prefix">
                <option value="Mr.">Mr.</option>
                <option value="Mrs.">Mrs.</option>
                <option value="Ms.">Ms.</option>
                <option value="Dr.">Dr.</option>
              </select>
            </div>
            <div class="col-sm-3">
                <input type="text" class="form-control" id="name" name="fname" placeholder="First name" required>
            </div>
            <div class="col-sm-3">
              <input type="text"  class="form-control" id="name" name="mname" placeholder="Middle name(Optional)" >
            </div>
            <div class="col-sm-3" >
                <input type="text" class="form-control" id="name" name="lname" placeholder="Last name" required> 
            </div>
          </div>
        </div>
      </div>
      <!--  HTML5 input email-->
      <div class="form-group">
        <label class="control-label col-sm-2" for="email">Email: <span class='inputInfo'>*</span></label>
        <div class="col-sm-10">
          <input type="email" class="form-control" id="email" name="email" placeholder="Enter email" required>
        </div>
      </div>
      <!--  HTML5 input Password-->
      <div class="form-group">
        <label class="control-label col-sm-2" for="pwd">Password: <span class='inputInfo'>*</span></label>
        <div class="col-sm-10"> 
          <input type="password" class="form-control" id="pwd" name="psw" placeholder="Enter password" title="minimum 8 length having uppercase , lowercase , number , special characters." required>
        </div>
      </div>
      <!-- HTML5 input password for Cofirm password -->
      <div class="form-group">
        <label class="control-label col-sm-2" for="conpwd">Confirm Password: <span class='inputInfo'>*</span></label>
        <div class="col-sm-10"> 
          <input type="password" class="form-control" id="conpwd" name="conPsw" placeholder="Confirm password" title="must be same as Password" required>
        </div>
      </div>
      <!-- Date of birth  type Date-->
      <div class="form-group">
        <label class="control-label col-sm-2" for="DOB">Date of Birth :</label>
        <div class="col-sm-10"> 
          <input type="date" class="form-control" id="DOB" name="DOB" placeholder="Date of Birth" title="Use calender to browse date">
        </div>
      </div>
      <!-- Seperate Drop Downs inputs for Marital Status and gender -->
      <div class="form-group">
        <label class="control-label col-sm-2" for="maritalStatus">Marital Status:</label>
        <div class="col-sm-4"> 
              <select class="form-control" name="maritalStatus" id="maritalStatus">
                <option value="Single">Single</option>
                <option value="Married">Married</option>
                <option value="Widowed">Widowed</option>
                <option value="Divorced">Divorced</option>
              </select>
        </div>      
        <label class="control-label col-sm-2" for="gender">Gender :</label>
        <div class="col-sm-4"> 
              <select class="form-control" name="gender" id="gender">
                <option value="Male">Male</option>
                <option value="Female">Female</option>
              </select>
        </div>
      </div>
      <!-- Organization and Designation input text -->
      <div class="form-group">
        <label class="control-label col-sm-2" for="organization">Organization : <span class='inputInfo'>*</span></label>
        <div class="col-sm-4"> 
            <input type="text" class="form-control" id="organization" name="organization" placeholder="Organization Name" title="Enter the organization name where you work" required>
        </div>      
        <label class="control-label col-sm-2" for="role">Designation : <span class='inputInfo'>*</span></label>
        <div class="col-sm-4"> 
               <input type="text" class="form-control" id="role" name="role" placeholder="Enter Designation " title="Enter the role as you are working in" required>
        </div>
      </div>
      <!-- Phone and fax input -->
      <div class="form-group">
        <label class="control-label col-sm-2" for="phone">Phone : <span class='inputInfo'>*</span></label>
        <div class="col-sm-4"> 
            <input type="text" class="form-control" id="phone" name="phone" placeholder="Phone or Mobile Number" title="only numbers" required>
        </div>      
        <label class="control-label col-sm-2" for="fax">Fax :</label>
        <div class="col-sm-4"> 
               <input type="text" class="form-control" id="fax" name="fax" placeholder="Enter Fax" title="Enter the fax number">
        </div>
      </div>
      <br/>
      <!-- Address with address type dropdown, street --> 
      <div class="form-group">
        <label class="control-label col-sm-2" for="addressType">Address Type :</label>
        <div class="col-sm-4"> 
            <select class="form-control" name="addressType" id="addressType">
                <option value="Office">Official</option>
                <option value="Residence">Residential</option>
              </select>
        </div>
        <label class="control-label col-sm-2" for="street">Street : </label>
        <div class="col-sm-4">
            <input type="text" class="form-control" id="street" name="street" placeholder="Enter Street Address/Details" title="local address keywords">
        </div>
      </div>
      <!-- Input city and zipcode -->
      <div class="form-group">
        <label class="control-label col-sm-2" for="city">City : <span style="color:red">*</span></label>
        <div class="col-sm-4">
            <input type="text" class="form-control" id="city" name="city" placeholder="Enter City Name" title="In case of Rural area, Enter nearest City" required>
        </div>
        <label class="control-label col-sm-2" for="zipCode">Zip Code : </label>
        <div class="col-sm-4">
            <input type="text" class="form-control" id="zipCode" name="zipCode" placeholder="Enter Zip Code" title="6 digit number">
        </div>
      </div>
      <!-- Input for State and Country -->
      <div class="form-group">
        <label class="control-label col-sm-2" for="state">State : <span style="color:red">*</span></label>
        <div class="col-sm-4">
            <input type="text" class="form-control" id="state" name="state" placeholder="Enter State Name" required>
        </div>
        <label class="control-label col-sm-2" for="country">Country : <span class='inputInfo'">*</span> </label>
        <div class="col-sm-4">
            <input type="text" class="form-control" id="country" name="country" placeholder="Enter Country Name" title="Must be a valid, Existing country" required>
        </div>
      </div>
      <!-- Input Textarea for extra note -->
      <div class="form-group">
        <label class="control-label col-sm-2" for="extraNote">Extra Note : <span class='inputInfo'>*</span></label>
        <div class="col-sm-10">
            <textarea rows="5" class="form-control" id="extraNote" name="extraNote" placeholder="Put something about yourself (Skills, hobby, preferences)" > </textarea>
        </div>
      </div>
      <!-- Submit button -->
      <div class="form-group"> 
        <div class="col-sm-offset-2 col-sm-10">
          <button type="submit" name="Submit" class="btn btn-primary btn-block">Submit</button>
        </div>
      </div>
      <!-- Side links for index.php(Login) and forgotPassword --> 
      <div class="form-group sideLinks">
        <a class="h5" href="index.php">Sign In here!</a>
        <span class="h5">Forgot <a href="ForgotPassword.php">password?</a></span>
      </div>
      </fieldset>
    </form>
</div>
</body>