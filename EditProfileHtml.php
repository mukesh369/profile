<body>
          <?php require "NavBar.php"; ?>
          <div class="container-fluid">
            <div class="row-fluid">
             <div class="col-sm-2 sidebar">
                <ul class="nav nav-sidebar">
                  <li><a href="#personal">Personal information</a></li>
                  <li><a href="#professional">Professional profile</a></li>
                  <li><a href="#address">Update Address &amp; Contacts</a></li>
                </ul>  
              </div>

              <div class="col-sm-10" id="edit">

                <!-- To show the alert message -->
                <div class="alert" id="message"></div>

                <!-- Personal info edit form -->
                <form class="form-horizontal" method="POST" id="personal" action="EditPersonalProfile.php">
                  <div class="pull-right"><span class="inputInfo">*</span> Required</div>
                  <legend>Update personal Info : </legend>
                  <fieldset>
                  <input type="hidden" name="csrf" value="<?php echo $_SESSION['csrf_token_editPersonal']; ?>">
                  <div class="form-group">

                    <!-- Name input , Divide as prefix, firstName, MiddleName, LastName -->
                    <label class="control-label col-sm-2" for="name">Name: <span class="inputInfo">*</span></label>
                    <div class="col-sm-10">
                      <div class="row">
                        <div class="col-sm-3">

                          <!-- Prefix -->
                          <select class="form-control" name="prefix">
                            <?php
                              // Creating Prefix Drop Down selecting the value of user
                             $prefix =  $emp->getValues("Prefix");
                              foreach ($prefix as $a) {
                                  if ($a==$emp->prefix) {
                                      echo "<option value='".$a."' selected>".$a."</option>";
                                  } else {
                                      echo "<option value='".$a."'>".$a."</option>";
                                  }
                              } ?>
                          </select>
                        </div>

                        <!-- First Name -->
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="name" name="fname" value="<?php echo $emp->firstName ; ?>" placeholder="First name" required>
                        </div>

                        <!-- Middle Name -->
                        <div class="col-sm-3">
                          <input type="text"  class="form-control" id="name" name="mname" value="<?php echo $emp->middleName ; ?>" placeholder="Middle name(Optional)" >
                        </div>

                        <!-- Last Name -->
                        <div class="col-sm-3" >
                            <input type="text" class="form-control" id="name" name="lname" value="<?php echo $emp->lastName ; ?>" placeholder="Last name" required> 
                        </div>
                      </div>
                    </div>
                  </div>

                  <!-- Email -->
                  <div class="form-group">
                    <label class="control-label col-sm-2" for="email">Email: <span class="inputInfo">*</span></label>
                    <div class="col-sm-10">
                      <input type="email" class="form-control" id="email" name="email" value="<?php echo $emp->email ; ?>" placeholder="Enter email" required>
                    </div>
                  </div>

                  <!-- Date of birth -->
                  <div class="form-group">
                    <label class="control-label col-sm-2" for="DOB">Date of Birth :</label>
                    <div class="col-sm-10"> 
                      <input type="date" class="form-control" id="DOB" name="DOB" value=<?php echo $emp->DOB; ?> placeholder="Date of Birth" title="Use Calender to browse Date">
                    </div>
                  </div>

                  <!-- Marital Status -->
                  <div class="form-group">
                    <label class="control-label col-sm-2" for="maritalStatus">Marital Status:</label>
                    <div class="col-sm-4"> 
                          <select class="form-control" name="maritalStatus" id="maritalStatus">
                            <?php
                             // Creating maritalStatus Drop Down selecting the value of user
                            $maritalStatus = $emp->getValues("MaritalStatus");
                              foreach ($maritalStatus as $a) {
                                  if ($a==$emp->maritalStatus) {
                                      echo "<option value='".$a."' selected>".$a."</option>";
                                  } else {
                                      echo "<option value='".$a."'>".$a."</option>";
                                  }
                              } ?>
                          </select>
                    </div>  
                    <!-- Gender -->    
                    <label class="control-label col-sm-2" for="gender">Gender :</label>
                    <div class="col-sm-4"> 
                          <select class="form-control" name="gender" id="gender">
                            <?php
                             // Creating Gender Drop Down selecting the value of user
                              $gender = $emp->getValues("Gender");
                              foreach ($gender as $a) {
                                  if ($a==$emp->gender) {
                                      echo "<option value='".$a."' selected>".$a."</option>";
                                  } else {
                                      echo "<option value='".$a."'>".$a."</option>";
                                  }
                              } ?>
                          </select>
                    </div>
                  </div>
                  <div class="form-group">

                    <!-- Extra Note -->
                    <label class="control-label col-sm-2" for="extraNote">Extra Note :</label>
                    <div class="col-sm-10">
                        <textarea rows="5" class="form-control" id="extraNote" name="extraNote" value="<?php echo $emp->extraNote ; ?>" title="Put something about yourself (Skills, hobby, preferences)" > </textarea>
                    </div>
                  </div>
                  <div class="form-group"> 
                    <div class="col-sm-offset-2 col-sm-10">
                      <button type="submit" name="Submit" class="btn btn-primary btn-block">Submit</button>
                    </div>
                  </div>
                  </fieldset>
                  </form>

                  <!--  Edit Address and Contact -->
                  <form class="form-horizontal" method="POST" id="address" action="EditAddressContacts.php">
                    <legend>Update Address and Contacts : </legend>
                  <fieldset>
                  <div class="form-group">
                    <input type="hidden" name="csrf" value="<?php echo $_SESSION['csrf_token_editAddress']; ?>">

                    <!-- Phone -->
                    <label class="control-label col-sm-2" for="phone">Phone : <span class="inputInfo">*</span></label>
                    <div class="col-sm-4"> 
                        <input type="text" class="form-control" id="phone" name="phone" placeholder="Phone or Mobile Number" title="only numbers" value="<?php echo $emp->phone ; ?>" required>
                    </div>      

                    <!-- Fax -->
                    <label class="control-label col-sm-2" for="fax">Fax :</label>
                    <div class="col-sm-4"> 
                           <input type="text" class="form-control" id="fax" name="fax" value="<?php echo $emp->fax ; ?>" placeholder="Enter Fax" title="Enter the fax number">
                    </div>
                  </div>
                  <div class="form-group">

                    <!-- Address Type -->
                    <label class="control-label col-sm-2" for="addressType">Address Type :</label>
                    <div class="col-sm-4"> 
                        <select class="form-control" name="addressType" id="addressType">
                            <?php
                             // Creating Address Type Drop Down selecting the value of user
                              $addressType = $emp->getValues("Type", "Address");
                              foreach ($addressType as $a) {
                                  if ($a==$emp->addressType) {
                                      echo "<option value='".$a."' selected>".$a."</option>";
                                  } else {
                                      echo "<option value='".$a."'>".$a."</option>";
                                  }
                              } ?>
                          </select>
                    </div>

                    <!-- Street -->
                    <label class="control-label col-sm-2" for="street">Street : </label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" id="street" name="street" value="<?php echo $emp->street ; ?>" placeholder="Enter Street Address/Details" title="local address keywords">
                    </div>
                  </div>
                  <div class="form-group">
                    
                    <!-- City -->
                    <label class="control-label col-sm-2" for="city">City : <span class="inputInfo">*</span></label>
                    
                    <div class="col-sm-4">
                        <input type="text" class="form-control" id="city" name="city" value="<?php echo $emp->city ; ?>" placeholder="Enter City Name" title="In case of Rural area, Enter nearest City" required>
                    </div>

                    <!-- Zip code -->
                    <label class="control-label col-sm-2" for="zipCode">Zip Code : </label>
                    
                    <div class="col-sm-4">
                        <input type="text" class="form-control" id="zipCode" name="zipCode" value="<?php echo $emp->zip ; ?>" placeholder="Enter Zip Code" title="6 digit number">
                    </div>
                  </div>


                  <div class="form-group">
                    <!-- State -->
                    <label class="control-label col-sm-2" for="state">State : <span class="inputInfo" >*</span></label>
                    
                    <div class="col-sm-4">
                        <input type="text" class="form-control" id="state" name="state" value="<?php echo $emp->state ; ?>" placeholder="Enter State Name" required>
                    </div>
                  </div>

                  <!-- Submit Button -->
                  <div class="form-group"> 
                    <div class="col-sm-offset-2 col-sm-10">
                      <button type="submit" name="Submit" class="btn btn-primary btn-block">Submit</button>
                    </div>
                  </div>
                  </fieldset>
                  </form>


                  <!-- Professional info edit form -->
                  <form class="form-horizontal" method="POST" id="professional" action="EditProfessionalInfo.php">
                    <legend>Update Professional Information : </legend>
                    <fieldset>
                    <input type="hidden" name="csrf" value="<?php echo $_SESSION['csrf_token_editProfessional']; ?>">
                    <!-- Organization -->
                    <div class="form-group">
                      <label class="control-label col-sm-4" for="organization">Organization : <span class="inputInfo">*</span></label>
                      <div class="col-sm-8"> 
                          <input type="text" class="form-control" id="organization" name="organization" placeholder="Company or Organization where you work" title="" value="<?php echo $emp->organization ; ?>" required>
                      </div> 
                    </div>

                    <!-- Designation -->
                    <div class="form-group">     
                      <label class="control-label col-sm-4" for="designation">Designation : <span class="inputInfo">*</span></label>
                      <div class="col-sm-8"> 
                             <input type="text" class="form-control" id="designation" name="role" value="<?php echo $emp->role ; ?>" placeholder="The Position or Role that you work as" required>
                      </div>
                    </div>
                    <div class="form-group"> 
                      <div class="col-sm-offset-4 col-sm-8">
                        <button type="submit" name="Submit" class="btn btn-primary btn-block">Submit</button>
                      </div>
                    </div>
                    </fieldset>
                </form>
              </div>
            </div>
          </div>
      </body>