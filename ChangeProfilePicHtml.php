<button data-toggle="collapse" data-target="#ChangeProfilePic">Change profile picture</button>
        <div id="ChangeProfilePic" class="collapse">
            <form class="form-horizontal" id="changedpform" method="POST" action="UploadProfilePictureAjax.php" enctype="multipart/form-data" >

                <div class="alert" id="message"></div>
                <input type="hidden" name="csrf" value="<?php echo $_SESSION["csrf_token_changePic"]?>">
                <div class="form-group">
                <label class="control-label col-sm-2" for="profilephoto">Profile Photo: </label>
                <div class="col-sm-10"> 
                  <input type="file" class="" id="profilephoto" name="profilePhoto" placeholder="Profile Photo" title="must be a  jpeg/jpg image">
                </div>
                </div>
                <div class="form-group"> 
                  <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" name="Submit" class="btn btn-primary btn-block">Submit</button>
                  </div>
                </div>
                
            </form>
        </div>