<?php

// Main profile (home page) shows user's all details and links for navigation.
session_start();

$_SESSION['csrf_token_changePic'] = md5(uniqid(rand(), true));

if (isset($_SESSION["userid"])) {
    include "Objects/Employee.php";
    include "Config/Database.php";
    include "Utilities/Validation.php";
    
    $database = new Database();
    $db = $database->getConnection();
    
    $emp = new Employee($db);
    // Get all the user information personal, professional, address and contacts.
    $msg = $emp->getProfileInfo($_SESSION["userid"]);
    
    if ($msg) {
        include "Header.php" ; ?>

<body>
    <?php include "NavBar.php"; ?>
    <h2>
        <center> 
            Welcome  <?php  if ($msg) {
                echo $emp->firstName;
            } ?>
        </center>
    </h2>
    <div id="id01" class="container-fluid">
        <?php
          if ($msg && !empty($emp->profilePhoto)) {
              // Show the profile picture
              echo "<img class='pull-left' id='profilePic' src='ImgShow.php?name=".$emp->profilePhoto."' alt=''/>";
          } ?>
        
        <?php include "UserIcon.php" ; ?>

        <!-- Display user's address and its location on google map -->
        <table class="pull-right" id="roleTable">
              <tr>
                <td>Address : </td>
                <td>
                    <?php $address=$emp->street.",".$emp->city." ,".$emp->state.", ".$emp->country;
                      echo $address ;
                    ?>
                </td>
              </tr>
              <!-- Google map location of address -->
              <tr><td colspan="2"><?php echo '<iframe id="iframe1" src="https://www.google.com/maps/embed/v1/place?key=AIzaSyDP1oRV7pFZPiFTzbUdfvbUoz6Bu_fa75w&q='.$address.'" allowfullscreen></iframe>' ; ?></td>
              </tr>
        </table>
    </div>


    <div class="jumbotron" id="profileInfo">
        <?php
        if ($msg) {
          ?>

                <h2><?php echo $emp->prefix." ".$emp->firstName." ".$emp->middleName." ".$emp->lastName ; ?></h2>
                <p><?php echo $emp->email ; ?></p>
                <p><?php echo $emp->maritalStatus ; ?></p>
                <p><?php
                     if ($emp->DOB!="0000-00-00") {
                        // Display date in d/m/Y format
                        $date=date_create($emp->DOB);
                        echo date_format($date,"d/m/Y");
                      } ?>
                  </p>
                <p><?php echo $emp->gender ; ?></p>
                <p><?php echo $emp->role ; ?></p>
                <p><?php echo $emp->organization ; ?></p>
              </div>
            
        <?php
              if ("0" == $emp->isAccountActivated) {
                  
                  require "Utilities/Security.php";
                  $security = new Security();
                  $encryptedId = $security->hash('encrypt', $_SESSION["userid"]);

                  $href = "AccountActivation.php?id=$encryptedId";
                  $msg = "Please activate the account from <a  href=$href><b> here.</b></a>.";
                  Validation::display();
              }
        } else {
            Validation::display("No Data found with the associated id");
        }

        include "ChangeProfilePicHtml.php";
        ?>
 <script type="text/javascript" src="Static/Js/ImgUploadViaAjax.js"></script>    
</body>
</html> 
<?php
    }
} else {
    header("Location:index.php");
} ?>