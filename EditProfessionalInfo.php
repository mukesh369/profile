<?php
//The page handles request submitted via Ajax for edit professional info and submits the data to server and returns the response to client
session_start() ;

if (isset($_SESSION["userid"]) && $_POST['csrf'] == $_SESSION['csrf_token_editProfessional']) {
    if ($_POST["organization"]!="" && $_POST["role"]!="") {
        require "Config/Database.php" ;
        require "Utilities/Validation.php";
        $database = new Database();
        $db = $database->getConnection();

        require "Objects/Employee.php";
        $emp = new Employee($db);
        
        $validation = new Validation();
        
        $emp->organization = $validation->testInput($_POST["organization"]);
        $emp->role = $validation->testInput($_POST["role"]);
        
        $emp->recordId = $_SESSION["userid"];
        
        $f = $emp->updateProfessionalProfile() ;
        
        if ($f) {
            $recordId = $emp->recordId;
            Validation::display("Professional Data Updated");
            header("refresh:2,url=EditProfile.php");
        } elseif (!$f) {
            Validation::display("Records not Updated");
            header("refresh:4,url=EditProfile.php");
        } else {
            Validation::display("Exception : ".$f);
        }
    } else {
        header("Location:EditProfile.php?alert='Please fill all the details first.'");
    }
} else {
    header("Location:EditProfile.php?alert='Please submit the form first.'");
}
