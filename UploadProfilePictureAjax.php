<?php session_start();

require 'Utilities/Validation.php';
require "Objects/Employee.php";
require "Config/Database.php";
// This page handles the request to upload the image  ,submit the data and returns the response to client.
if (isset($_SESSION["userid"]) && ($_POST['csrf'] == $_SESSION['csrf_token_changePic'] || $_POST['csrf'] == $_SESSION['csrf_token_dP'])) {
    $target_dir = "Uploads/";

    $database = new Database();
    $db = $database->getConnection();
    
    // Checking file name . file-0 implies request is coming from another page via ajax while profilePhoto implies request is coming from another page.
    if (isset($_FILES["file-0"]["name"])) {
        $file="file-0";
    } else {
        $file="profilePhoto";
    }

    // File url that is going to be stored on server - uploads/pic_userid_date_filename
    $target_file = $target_dir . "pic_".$_SESSION["userid"]."_".date("Y-m-d")."_".basename($_FILES[$file]["name"]);
    
    // Upload flag.
    $uploadOk = 1;

    // Checking file type
    $imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);
    
    if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
    && $imageFileType != "gif" ) {
        echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
        $uploadOk = 0;
    }

    // check if file is an actual image or not.
    $check = getimagesize($_FILES[$file]["tmp_name"]) ;
    if (!$check) {
        echo "File is not an image.";
        $uploadOk = 0;
    }

    // Check if file already exists
    if (file_exists($target_file)) {
        echo "Sorry, file already exists.";
        

        // if file already exists then don't move the image to server but update the user's information to that file.
        
        $emp = new Employee($db);
        $msg = $emp->updateInfo($_SESSION["userid"], "PhotoPath", $target_file);
        if (!$msg) {
            if (!unlink($target_file)) {
                echo "File could not be uploaded and could not be deleted from moved directory.";
            } else {
                //If file could not be uploaded then delete the file from server.
                echo "Sorry , File could not be uploaded on server.";
            }
        } else {
            echo "Congratulations. Your profile photo has been successfully updated.";
            header("refresh:4,url=Profile.php");
        }
        $uploadOk = 0;
    }
    // Check file size, should not be more than 500KB
    if ($_FILES[$file]["size"] > 500000) {
        echo "Sorry, your file is too large.";
        $uploadOk = 0;
    }

    // Check if $uploadOk is set to 0 by an error
    if (0 == $uploadOk) {
        echo "Sorry, your file was not uploaded.";
        // if everything is ok, try to upload file
    } else {
        if (move_uploaded_file($_FILES[$file]["tmp_name"], $target_file)) {
            $emp = new Employee($db);
            $msg = $emp->updateInfo($_SESSION["userid"], "PhotoPath", $target_file);
            if (!$msg) {
                if (!unlink($target_file)) {
                    $msg = "File could not be uploaded and could not be deleted from moved directory.";
                } else {
                    $msg = "Sorry , File could not be uploaded on server.";
                }
                Validation::display($msg);
            } else {
                $msg = "Congratulations. Your profile photo has been successfully updated.";
                Validation::display($msg);
                header("refresh:4,url=Profile.php");
            }
        } else {
            // Error in moving the file to server directory
            Validation::display("Sorry, There was an error uploading your file");
        }
    }
} else {
    Validation::display("Session is not set.");
}
