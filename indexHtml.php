<body>
<h2 class="" ><center>Log In</center></h2>
<div id="id01" class="container-fluid">
    <?php include "UserIcon.php" ; ?>
    <!-- Log in Form -->
    <form class="form-horizontal" id="loginForm" method="POST" action="Authenticate.php">
      <fieldset>
      <!--CSRF Token-->
      <input type="hidden" name="csrf_token" value="<?php echo $_SESSION["csrf_token_signIn"];?>">
      <!-- input email -->
      <div class="form-group">
        <label class="control-label col-sm-2" for="email">Email:</label>
        <div class="col-sm-10">
          <input type="email" class="form-control" id="email" name="email" placeholder="Enter email" required>
        </div>
      </div>
      <!-- input password -->
      <div class="form-group">
        <label class="control-label col-sm-2" for="pwd">Password:</label>
        <div class="col-sm-10"> 
          <input type="password" name="psw" class="form-control" id="psw" placeholder="Enter password" required>
        </div>
      </div>
      <!-- Remember me checkbox -->
      <div class="form-group checkbox" id="rememberMe">
        <label class="control-label col-sm-offset-1 col-sm-3" ><input type="checkbox" name="remember_me"> Remember me</label>
      </div>
      <!-- Submit button -->
      <div class="form-group"> 
        <div class="col-sm-offset-2 col-sm-10">
          <button type="submit" name="Submit" class="btn btn-primary btn-block">Submit</button>
        </div>
      </div>
      <!-- Sign up and forgot password links -->
      <div class="form-group sidelinks" >
        <a class="h5" href="SignUp.php">Sign Up here!</a>
        <span class="h5">Forgot <a href="ForgotPassword.php">password?</a></span>
      </div>
      </fieldset>
    </form>
</div>
</body>