<?php
// This page handles the request for change password from ChangePassword.php and submits the data to the server and returns the response to client.
session_start() ;

if (isset($_POST["Submit"]) && $_POST['csrf'] == $_SESSION['csrf_token_changePsw']) {
    if ($_POST["id"]!="" && $_POST["psw"]!="" && $_POST["conPsw"]!="") {
        // Server Side validation of password and confirmPassword.
        $regex= "^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})";
        $f1 = preg_match($regex, $_POST["psw"]);
        $f2 = preg_match($regex, $_POST["conPsw"]);

        // Password matching on  server side
        if ($f1 && $f2 && $_POST["psw"] != $_POST["conPsw"]) {
            header("Location:ChangePassword.php?alert='Password and Confirm password does not match on server.'");
        }
     
        require "Config/Database.php";
        $database=new Database();
        $db=$database->getConnection();

        require "Objects/Employee.php";
        $emp=new Employee($db);

        require "Utilities/Validation.php";
        $validation=new Validation();
        // Validating input for trimming and specialchars.
        $id = $validation->testInput($_POST["id"]);
        $psw = $validation->testInput($_POST["psw"]);

        // Create password hash before storing.
        $psw = password_hash($psw, PASSWORD_DEFAULT) ;

        // Update the password
        $update=$emp->updateInfo($id, "Password", $psw);
        if ($update) {
            $msg = "Congratulations. Your password has been successfully update";
            Validation::display($msg);
            header("refresh:4;url=index.php");
        } elseif (!$update) {
            $msg = "No Account exists with the given id .";
            Validation::display($msg);
            header("refresh:4;url=index.php") ;
        } else {
            Validation::display($update);
            header("refresh:4;url=index.php") ;
        }
    } else {
        //if required details are not set, redirect it back.
        header("Location:ChangePassword.php?alert='Please fill all the details first.'");
    }
} else {
    header("Location:ForgotPassword.php?alert='Please submit the details first.'");
}
