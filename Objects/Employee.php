<?php
class Employee
{
    //private data
    private $conn;
    private $tableName = 'Employee';
   
    //public data
    public $id;
    public $prefix;
    public $firstName;
    public $middleName;
    public $lastName;
    public $email;
    public $password;
    public $profilePhoto;
    public $maritalStatus;
    public $DOB;
    public $gender;
    public $extraNote;
    public $organizationId;
    public $roleId;

    public $organization;
    public $role;

    public $addressType;
    public $street;
    public $zip;
    public $phone;
    public $fax;
    public $city;
    public $state;
    public $country;

    public $recordId;
    public $isAccountActivated;
    
    /*
    * Constructor
    * params1 $db --> database connection
    * sets db connection with the class
    */
    public function __construct($db)
    {
        $this->conn = $db;
        ini_set("log_errors", 1);
        ini_set("error_log", "/var/www/html/profileapp/Logs/Error.log");
    }



    /*
    * authentication.
    * Fetches id, password of user with email. encrypt the entered password and match with fetched password.
    * if matched then authenticated else returns false
    */
    public function signIn()
    {
        try {
            $query = "SELECT PK_ID , password FROM  " . $this->tableName . " WHERE Email =:email";

            $stmt = $this->conn->prepare($query);
            $stmt->bindParam(":email", $this->email);
            $stmt->execute();
            $rowCount = $stmt->rowCount();
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
            $this->id = $row['PK_ID'];
            return (password_verify($this->password, $row['password']));
        } catch (PDOException $event) {
            error_log($event->getMessage());
            return 'Sorry! We could not find your profile.' ;
        }
    }



    /* Sign up
    * Insert a new employee record in the table
    * We have to fill data in many tables. So we are using flags to proceed to next step if previous one is done.
    * first insert into country, state,city then organization and designation then  insert personal information after that Insert into address table.
    * State requires country id, city requires state id, employee requires role and organization id, address requires employee id. So that's how the insertion order followed.
    */
    public function signUp()
    {
        $this->conn->beginTransaction();
        try {
            // Insert into Country table if it is not there already
            $query1 = "INSERT IGNORE INTO Country SET Name=:country";
            $stmt1 = $this->conn->prepare($query1);
            $stmt1->bindParam(":country", $this->country);
            $stmt1->execute();

            // Insert into State table if it is not there already
            $query2 = "INSERT IGNORE INTO State SET Name=:state, FK_Country_ID=(SELECT Country.PK_ID FROM Country WHERE Name=:country LIMIT 1)";
            $stmt2 = $this->conn->prepare($query2);
            $stmt2->bindParam(":state", $this->state);
            $stmt2->bindParam(":country", $this->country);
            $stmt2->execute();

            // Insert into City Table if it is not there already
            $query3 = "INSERT IGNORE INTO City SET Name=:city, FK_State_ID=(SELECT State.PK_ID FROM State WHERE Name=:state LIMIT 1)";
            $stmt3 = $this->conn->prepare($query3);
            $stmt3->bindParam(":city", $this->city);
            $stmt3->bindParam(":state", $this->state);
            $stmt3->execute();
                
            
            // Country, State, City is inserted. Let's insert professional info.
            // Insert into organization if it is not there already.
            $query4 = "INSERT IGNORE INTO Organization SET Name=:organization";
            $stmt4 = $this->conn->prepare($query4);
            $stmt4->bindParam(":organization", $this->organization);
            $stmt4->execute();

            // Insert into Role table if it is new.
            $query5 = "INSERT IGNORE INTO Role SET Name=:role";
            $stmt5 = $this->conn->prepare($query5);
            $stmt5->bindParam(":role", $this->role);
            $stmt5->execute();

            // Create a entry in mapping table ignoring repetition
            $query6 = "INSERT IGNORE INTO OrganizationRole SET FK_Organization_ID= (SELECT Organization.PK_ID FROM Organization WHERE Name=:organization LIMIT 1), FK_Role_ID = (SELECT Role.PK_ID FROM Role WHERE Name=:role LIMIT 1)";
            $stmt6 = $this->conn->prepare($query6);
            $stmt6->bindParam(":organization", $this->organization);
            $stmt6->bindParam(":role", $this->role);
            $stmt6->execute();
            
            // Insert into Employee table personal data if it is not there already
            $query7 = "INSERT IGNORE INTO 
            		Employee
            		SET prefix=:prefix, firstName=:fname, middleName=:mname, lastName=:lname, email=:email, password=:password, maritalStatus=:maritalStatus, DOB=:DOB, gender=:gender, extraNote=:extraNote, FK_Organization_ID=(SELECT Organization.PK_ID FROM Organization WHERE Name=:organization LIMIT 1), FK_Role_ID=(SELECT Role.PK_ID FROM Role WHERE Name=:role LIMIT 1)
                        ";
            $stmt7 = $this->conn->prepare($query7);

            // Bind the parameters
            $stmt7->bindParam(":prefix", $this->prefix);
            $stmt7->bindParam(":fname", $this->firstName);
            $stmt7->bindParam(":mname", $this->middleName);
            $stmt7->bindParam(":lname", $this->lastName);
            $stmt7->bindParam(":email", $this->email);
            // Hash the password by using salt PASSWORD_DEFAULT.
            $stmt7->bindParam(":password", password_hash($this->password, PASSWORD_DEFAULT));
            $stmt7->bindParam(":maritalStatus", $this->maritalStatus);
            $stmt7->bindParam(":DOB", $this->DOB);
            $stmt7->bindParam(":gender", $this->gender);
            $stmt7->bindParam(":extraNote", $this->extraNote);
            $stmt7->bindParam(":organization", $this->organization);
            $stmt7->bindParam(":role", $this->role);

            if ($stmt7->execute()) {
                $this->recordId = $this->conn->lastInsertId();
                $affectedRows = $stmt7->rowCount();
                if (0 == $affectedRows) {
                    return false ;
                }
            } else {
                return false ;
            }
            
            $query8 = "INSERT IGNORE INTO Address SET Type=:type, Street=:street, Zip=:zip, Phone=:phone, Fax=:fax, FK_City_ID=(SELECT City.PK_ID FROM City WHERE Name=:city LIMIT 1), FK_Employee_ID=:recordId";
            $stmt8 = $this->conn->prepare($query8);
            // Bind address parameters
            $stmt8->bindParam(":type", $this->addressType);
            $stmt8->bindParam(":street", $this->street);
            $stmt8->bindParam(":zip", $this->zip);
            $stmt8->bindParam(":phone", $this->phone);
            $stmt8->bindParam(":fax", $this->fax);
            $stmt8->bindParam(":city", $this->city);
            $stmt8->bindParam(":recordId", $this->recordId);
            $stmt8->execute();
            $this->conn->commit();
            return true;
        } catch (PDOException $e) {
            $this->conn->rollBack();
            error_log($e->getMessage());
            return "There is some inconsistency with the data you submitted." ;
        }
    }


    /*
    * Params userid, key, value
    * Update the key column with value in the users row with the userid
    * returns true and false on success and fail.
    */
    public function updateInfo($userId, $key, $value)
    {
        try {
            $query = "UPDATE ".$this->tableName." SET ".$key." = :value WHERE PK_ID = :userId ";
            $stmt = $this->conn->prepare($query);

            $stmt->bindParam(":value", $value);
            $stmt->bindParam(":userId", $userId);

            if ($stmt->execute()) {
                $insertId = $this->conn->lastInsertId();
                $affectedRows = $stmt->rowCount();
                return (!(0 === $affectedRows));
            }
        } catch (PDOException $e) {
            error_log($e->getMessage());
            return "Sorry we could not update that. ";
        }
    }


    /*
    * Params userid. Takes the input and Activate the account.
    */
    public function activateAccount($userId)
    {
        try {
            $query = "SELECT PK_ID,IsAccountActivated from ".$this->tableName." WHERE PK_ID = ".$userId;
            $stmt = $this->conn->prepare($query);
            $stmt->execute() ;
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
            if ($stmt->rowCount()>0) {
                $userid = $row["PK_ID"];
                $isAccountActivated = $row["IsAccountActivated"];
                if ("0" == $isAccountActivated) {
                    $query1 = "UPDATE ".$this->tableName." SET IsAccountActivated= '1' WHERE PK_ID = ".$userid;
                    $stmt1 = $this->conn->prepare($query1);
                    $res = 0 ;
                    if ($stmt1->execute()) {
                        if ($stmt1->rowCount()>0) {
                            // Account activated.
                            $res = 1 ;
                        }
                    } else {
                        // Account could not be activated.
                        $res = -1 ;
                    }
                } else {
                    // Account is already activated.
                    $res = 0;
                }

            } else {
                // Profile not found.
                $res = 2 ;
            }

            return $res;
        } catch (PDOException $e) {
            error_log($e->getMessage());
            return "Hey! There is some conflicts occured. Will you help us to fix that for you!";
        }
    }


    /*
    * Custom method
    * Params Email. Fetches name and id of the user having the email.
    */
    public function getId($email)
    {
        try {
            $query = "SELECT PK_ID, firstName,middleName,lastName from ".$this->tableName." WHERE email = :email" ;

            $stmt = $this->conn->prepare($query) ;
            $stmt->bindParam(":email", $email);

            $stmt->execute() ;
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
            if ($stmt->rowCount() >0) {
                $this->id = $row["PK_ID"];
                $this->firstName = $row["firstName"];
                $this->lastName = $row["lastName"];
                $this->middleName = $row["middleName"];
                $this->email = $email;
                return true ;
            } else {
                return false ;
            }
        } catch (PDOException $e) {
            error_log($e->getMessage());
            return "Hey, Sorry I could not found your data";
        }
    }


    /*
    * Params userId.
    * Fetches user's personal, professional, address and contacts.
    */
    public function getProfileInfo($userId)
    {
        try {
            // Extract employee's personal/professional/Address/Contacts data from tables.
            // INNER JOIN on required fields, LEFT join on optional fields.

            $query = "SELECT Prefix, FirstName, MiddleName, LastName, Email, MaritalStatus,IsAccountActivated, DOB, PhotoPath, Gender, 
                ExtraNote, Role.Name AS Role, Organization.Name AS Organization, 
                Type , Street, Zip, Phone, Fax, City.Name AS City, 
                State.Name AS State, Country.Name AS Country, ExtraNote
                FROM Employee
                INNER JOIN Role ON Role.PK_ID = Employee.FK_Role_ID
                INNER JOIN Organization ON Employee.FK_Organization_ID = Organization.PK_ID
                LEFT JOIN (
                Address
                INNER JOIN City ON Address.FK_CITY_ID = City.PK_ID
                INNER JOIN State ON City.FK_State_ID = State.PK_ID
                INNER JOIN Country ON State.FK_Country_ID = Country.PK_ID
                ) ON Employee.PK_ID = Address.FK_Employee_ID
                WHERE Employee.PK_ID =".$userId;

            $stmt = $this->conn->prepare($query);
            
            if (!$stmt->execute()) {
                return false;
            } else {
                $rowCount = $stmt->rowCount();
                $row = $stmt->fetch(PDO::FETCH_ASSOC);

                // Setting object's properties and values.
                
                $this->prefix = $row['Prefix'];
                $this->firstName = $row['FirstName'];
                $this->middleName = $row['MiddleName'];
                $this->lastName = $row["LastName"];
                $this->email = $row["Email"];
                $this->isAccountActivated = $row["IsAccountActivated"];
                $this->maritalStatus = $row["MaritalStatus"];
                $this->DOB = $row["DOB"];
                $this->gender = $row["Gender"];
                $this->role = $row["Role"];
                $this->organization = $row["Organization"];
                $this->profilePhoto = $row["PhotoPath"];
                $this->phone = $row["Phone"];
                $this->role = $row["Role"];
                $this->organization = $row["Organization"];
                $this->addressType = $row["Type"];
                $this->street = $row["Street"];
                $this->zip = $row["Zip"];
                $this->extraNote = $row["ExtraNote"];
                $this->fax = $row["Fax"];
                $this->city = $row["City"];
                $this->state = $row["State"];
                $this->country = $row["Country"];
                $this->extraNote = $row["ExtraNote"];

                return true ;
            }
        } catch (PDOException $e) {
            error_log($e->getMessage());
            return "We are sorry, We could not find your data";
        }
    }

    /*
    * This method update the personal data.
    * Params userId whose data to update.
    * Params Data - Associative array with key value pair that represents the data to update with.
    * Returns true and false if success or fail.
    */
    public function updatePersonalProfile($userid, $data)
    {
        try {

            // creating the query as Update employee set 'Key'='value2', 'key2'='value2' to update the data.
            $query = "UPDATE Employee SET " ;
            foreach ($data as $key => $value) {
                if ($value!="") {
                    $query .= $key .  " = '" . $value . "', " ;
                }
            }

            // Remove last ', ' and add where clause in sql query.
            $query = rtrim($query, ", ");
            $query .= " WHERE PK_ID = ".$userid;

            $stmt = $this->conn->prepare($query);

            if ($stmt->execute()) {
                $insertId = $this->conn->lastInsertId();

                $affectedRows = $stmt->rowCount();
                return (!(0 == $affectedRows));
            }
        } catch (PDOException $e) {
            error_log($e->getMessage());
            return "Sorry. We can't update the data. ";
        }
    }

    /*
    * Updates the professional data
    * Params set from object's properties along with userid.
    * Update the professional information and return true if success, false on not updation
    * custom message on exception.
    */
    public function updateProfessionalProfile()
    {
        try {
            $query1 = "UPDATE Role SET Name=:role WHERE PK_ID = ( SELECT FK_Role_ID FROM Employee WHERE PK_ID = :id) ";
            $stmt1 = $this->conn->prepare($query1);
            $stmt1->bindParam(":role", $this->role);
            $stmt1->bindParam(":id", $this->recordId);
            if ($stmt1->execute()) {
                $query2 = "UPDATE Organization SET Name=:org WHERE PK_ID = (SELECT FK_Organization_ID FROM Employee WHERE PK_ID = :id) ";
                $stmt2=$this->conn->prepare($query2);
                $stmt2->bindParam(":org", $this->organization);
                $stmt2->bindParam(":id", $this->recordId);
                return $stmt2->execute();
            }
        } catch (PDOException $e) {
            error_log($e->getMessage());
            return "Sorry! Were you really sure you wanted to update the professional data? Well, Try again. Bad luck ";
        }
    }

    /*
    * update Address And contacts
    * params received from object's properties along with user id.
    * returns true on success and false on failure.
    * using flags to update the tables step by step.
    */
    public function updateAddress()
    {
        try {
            
            $flag = 1;
            $query1 = "UPDATE State SET Name = :state WHERE FK_Country_ID = (SELECT Country.PK_ID FROM Country WHERE Name = :country LIMIT 1)";
            $stmt1 = $this->conn->prepare($query1);
            $stmt1->bindParam(":state", $this->state);
            $stmt1->bindParam(":country", $this->country);
            if ($stmt1->execute()) {
                $flag = 2;
                $query2 = "UPDATE City SET Name = :city WHERE FK_State_ID = (SELECT State.PK_ID FROM State WHERE Name = :state LIMIT 1)";
                $stmt2 = $this->conn->prepare($query2);
                $stmt2->bindParam(":city", $this->city);
                $stmt2->bindParam(":state", $this->state);
                if ($stmt2->execute()) {
                    $flag = 3;
                }
            }
            
            if (3 === $flag) {
                $query3 = "UPDATE Address SET Type = :type, Street = :street, Zip = :zip, Phone =:phone, Fax=:fax, FK_City_ID=(SELECT City.PK_ID FROM City WHERE Name=:city LIMIT 1) WHERE FK_Employee_ID=:recordId";
                $stmt3=$this->conn->prepare($query3);
                $stmt3->bindParam(":type", $this->addressType);
                $stmt3->bindParam(":street", $this->street);
                $stmt3->bindParam(":zip", $this->zip);
                $stmt3->bindParam(":phone", $this->phone);
                $stmt3->bindParam(":fax", $this->fax);
                $stmt3->bindParam(":city", $this->city);
                $stmt3->bindParam(":recordId", $this->recordId);

                return $stmt3->execute();
            }
        } catch (PDOException $e) {
            error_log($e->getMessage());
            return  "Hey! Something happened in the way to update the data and return back to you. ";
        }
    }

    /*
    * gets the values of a given enum field in employee table and returns the enums values as array
    * @param $field is the enum field's name
    * return enum values as array 
    */
    public function getValues($field, $table = "Employee")
    {
        try {
            $query = "DESC $table :field";

            $stmt = $this->conn->prepare($query);

            $stmt->bindParam(":field", $field);
            
            if ($stmt->execute())
            {
                $row = $stmt->fetch(PDO::FETCH_OBJ);
                if ($row === false) 
                {
                    return false;
                }

                $type_dec = $row->Type;
                if (substr($type_dec, 0, 5) !== 'enum(')
                {
                    return false;
                }

                $values = array();
                foreach (explode(',', substr($type_dec, 5, (strlen($type_dec) - 6))) as $v)
                {
                    array_push($values, trim($v, "'"));
                }
                return $values;
            }
            return false;
        }
        catch(PDOException $e)
        {
            error_log($e->getMessage());
        }
    }
}
