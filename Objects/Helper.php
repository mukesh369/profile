<?php
require "/var/www/html/profileapp/Config/Database.php";
class Helper
{
	private $db;
	private $conn;


	public function __construct()
	{
		$this->db = new Database();
		$this->conn = $db->getConnection();
	}


	public function Insert($sql, $params)
	{
		$stmt=$this->conn->prepare($sql);
		foreach ($params as $key => $value) {
			$stmt->bindParam($key, $value);
        }
        return $stmt->execute();
	}

}