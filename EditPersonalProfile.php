<?php
//The page handles request for edit personal data and submits the data to server and returns the response to client
session_start() ;


if (isset($_POST["Submit"]) && isset($_SESSION["userid"]) && $_POST['csrf'] == $_SESSION['csrf_token_editPersonal']) {
    if ($_POST["fname"]!="" && $_POST["prefix"]!="" && $_POST["lname"]!="" && $_POST["email"]!="") {
        require "Config/Database.php" ;
        require "Utilities/Validation.php";
        $database=new Database();
        $db=$database->getConnection();

        require "Objects/Employee.php";
        
        $emp=new Employee($db);
        
        $validation=new Validation();
        
        $data= array();

        $obj = array("FirstName" => $_POST["fname"] , "MiddleName" => $_POST['mname'], "LastName" => $_POST["lname"], "Email" => $_POST["email"], "Prefix" => $_POST["prefix"], "DOB" => $_POST["DOB"], "MaritalStatus" => $_POST["maritalStatus"], "Gender" => $_POST["gender"], "extraNote" => $_POST["extraNote"]);
        $data = $validation->validate($obj, $data);

        $f = $emp->UpdatePersonalProfile($_SESSION['userid'], $data);
        
        if ($f) {
            Validation::display("Records updated.");
            header("refresh:4,url=EditProfile.php");
        } elseif (!$f) {
            Validation::display("Could not found the user id.");
            header("refresh:4,url=EditProfile.php");
        } else {
            Validation::display("Exception : ".$f);
        }
    } else {
        header("Location:EditProfile.php?alert='Please fill all the details first.'");
    }
} else {
    header("Location:EditProfile.php?alert='Please submit the form first.'");
}
