-- phpMyAdmin SQL Dump
-- version 4.6.6deb1+deb.cihar.com~trusty.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Nov 06, 2017 at 02:53 PM
-- Server version: 5.6.33-0ubuntu0.14.04.1-log
-- PHP Version: 5.5.9-1ubuntu4.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ProfileApp`
--

-- --------------------------------------------------------

--
-- Table structure for table `Address`
--

CREATE TABLE `Address` (
  `PK_ID` int(11) UNSIGNED NOT NULL,
  `Type` enum('Office','Residence') COLLATE utf8mb4_unicode_ci NOT NULL,
  `Street` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Zip` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Phone` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Fax` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `FK_City_ID` int(10) UNSIGNED NOT NULL,
  `FK_Employee_ID` int(10) UNSIGNED NOT NULL,
  `DateUpdated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `DateCreated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `Address`
--

INSERT INTO `Address` (`PK_ID`, `Type`, `Street`, `Zip`, `Phone`, `Fax`, `FK_City_ID`, `FK_Employee_ID`, `DateUpdated`, `DateCreated`) VALUES
(1, '', 'Shailashree Vihar', '750123', '8009121351', '8009121351', 1, 38, '2017-08-29 18:47:33', '2017-08-29 18:47:33'),
(2, 'Office', 'Shailashree Vihar', '750123', '7007886107', '8009121351', 1, 39, '2017-09-18 16:04:09', '2017-09-18 16:04:09'),
(3, 'Office', '', '', '123456789', '', 2, 40, '2017-11-06 11:21:58', '2017-11-06 11:21:58');

-- --------------------------------------------------------

--
-- Table structure for table `City`
--

CREATE TABLE `City` (
  `PK_ID` int(10) UNSIGNED NOT NULL,
  `Name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `FK_State_ID` int(10) UNSIGNED NOT NULL,
  `DateCreated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `DateUpdated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `City`
--

INSERT INTO `City` (`PK_ID`, `Name`, `FK_State_ID`, `DateCreated`, `DateUpdated`) VALUES
(1, 'Bhubaneswar', 1, '2017-08-29 17:58:01', '2017-08-29 17:58:01'),
(2, 'Las Angeles', 2, '2017-11-06 11:21:58', '2017-11-06 11:21:58');

-- --------------------------------------------------------

--
-- Table structure for table `Country`
--

CREATE TABLE `Country` (
  `PK_ID` int(10) UNSIGNED NOT NULL,
  `Name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `DateCreated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `DateUpdated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `Country`
--

INSERT INTO `Country` (`PK_ID`, `Name`, `DateCreated`, `DateUpdated`) VALUES
(1, 'India', '2017-08-29 17:58:01', '2017-08-29 17:58:01'),
(2, 'USA', '2017-11-06 11:21:58', '2017-11-06 11:21:58');

-- --------------------------------------------------------

--
-- Table structure for table `Employee`
--

CREATE TABLE `Employee` (
  `PK_ID` int(10) UNSIGNED NOT NULL,
  `Prefix` enum('Mr.','Mrs.','Ms.','Dr.') COLLATE utf8mb4_unicode_ci NOT NULL,
  `FirstName` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `MiddleName` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `LastName` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Email` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Password` varchar(512) COLLATE utf8mb4_unicode_ci NOT NULL,
  `IsAccountActivated` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `PhotoPath` varchar(512) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `MaritalStatus` enum('Single','Married','Widowed','Divorced') COLLATE utf8mb4_unicode_ci NOT NULL,
  `DOB` date NOT NULL,
  `Gender` enum('Male','Female') COLLATE utf8mb4_unicode_ci NOT NULL,
  `ExtraNote` text COLLATE utf8mb4_unicode_ci,
  `FK_Organization_ID` int(10) UNSIGNED NOT NULL,
  `FK_Role_ID` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `DateUpdated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `DateCreated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `Employee`
--

INSERT INTO `Employee` (`PK_ID`, `Prefix`, `FirstName`, `MiddleName`, `LastName`, `Email`, `Password`, `IsAccountActivated`, `PhotoPath`, `MaritalStatus`, `DOB`, `Gender`, `ExtraNote`, `FK_Organization_ID`, `FK_Role_ID`, `DateUpdated`, `DateCreated`) VALUES
(29, 'Mr.', 'Muzeebul', 'Hassan', 'Ansari', 'iammujeebul@gmail.com', '$2y$10$zrujS1y.4i5ItC/Dv1amgOwH.dCpbnLKTOc0WakYO.priwSvc.qM6', '1', NULL, 'Single', '0000-00-00', 'Male', NULL, 10, 7, '2017-08-28 00:15:44', '2017-08-28 00:15:44'),
(32, 'Mr.', 'Mukesh', 'Kumar', 'Srivastava', 'mukesh.s@mindfiresolutions.com', '$2y$10$qmgxwKzcmkRfn7MjTxldlOIdlLDAvNr6iFAjYjYenqW6kmXy4YVWG', '0', NULL, 'Single', '2017-08-12', 'Male', '', 10, 7, '2017-08-29 18:34:40', '2017-08-29 18:34:40'),
(38, 'Mr.', 'Mukesh', 'Kumar', 'Srivastav', 'mukeshsrivastav691@gmail.com', '$2y$10$UH9eDX6roDhQdGn6fKIZHut6WhIVEGRwk7IUa3Ufjba9W.8adcayC', '1', 'Uploads/pic_38_2017-11-06_Picture 057.jpg', 'Single', '1996-07-18', 'Male', 'I am good', 10, 7, '2017-08-29 18:47:33', '2017-08-29 18:47:33'),
(39, 'Mr.', 'saas', 'sasas', 'asa', 'dsds@ffdff.com', '$2y$10$ZJpQ3XbqFXiTArId3kewreg2tdUmU1aTNA5Ahpc/y/VsUNTqnGGZ.', '0', NULL, 'Single', '2000-01-01', 'Male', 'Let\'s check', 10, 7, '2017-09-18 16:04:09', '2017-09-18 16:04:09'),
(40, 'Mr.', 'John', NULL, 'Carter', 'johncarter@gmail.com', '$2y$10$csyQe/ghDbQLpHnYP7oenOQ1Oh7HoKbMyDVvUDPEya.yFTGvPb1JW', '0', NULL, 'Single', '2001-01-01', 'Male', 'Nice', 12, 8, '2017-11-06 11:21:58', '2017-11-06 11:21:58');

-- --------------------------------------------------------

--
-- Table structure for table `Organization`
--

CREATE TABLE `Organization` (
  `PK_ID` int(10) UNSIGNED NOT NULL,
  `Name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `DateCreated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `DateUpdated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `Organization`
--

INSERT INTO `Organization` (`PK_ID`, `Name`, `DateCreated`, `DateUpdated`) VALUES
(10, 'Mindfire Solutions', '2017-08-29 18:34:39', '2017-08-29 18:34:39'),
(11, 'Mindfire Solutions BBS', '2017-09-01 16:56:01', '2017-09-01 16:56:01'),
(12, 'Hollywood', '2017-11-06 11:21:58', '2017-11-06 11:21:58');

-- --------------------------------------------------------

--
-- Table structure for table `OrganizationRole`
--

CREATE TABLE `OrganizationRole` (
  `PK_ID` int(10) UNSIGNED NOT NULL,
  `FK_Organization_ID` int(10) UNSIGNED NOT NULL,
  `FK_Role_ID` int(10) UNSIGNED NOT NULL,
  `DateCreated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `DateUpdated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `OrganizationRole`
--

INSERT INTO `OrganizationRole` (`PK_ID`, `FK_Organization_ID`, `FK_Role_ID`, `DateCreated`, `DateUpdated`) VALUES
(7, 10, 7, '2017-08-29 18:34:40', '2017-08-29 18:34:40'),
(8, 11, 7, '2017-09-01 16:56:02', '2017-09-01 16:56:02'),
(9, 12, 8, '2017-11-06 11:21:58', '2017-11-06 11:21:58');

-- --------------------------------------------------------

--
-- Table structure for table `Role`
--

CREATE TABLE `Role` (
  `PK_ID` int(10) UNSIGNED NOT NULL,
  `Name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `DateCreated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `DateUpdated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `Role`
--

INSERT INTO `Role` (`PK_ID`, `Name`, `DateCreated`, `DateUpdated`) VALUES
(7, 'Software Engineer', '2017-08-29 18:34:39', '2017-08-29 18:34:39'),
(8, 'Actor', '2017-11-06 11:21:58', '2017-11-06 11:21:58');

-- --------------------------------------------------------

--
-- Table structure for table `State`
--

CREATE TABLE `State` (
  `PK_ID` int(10) UNSIGNED NOT NULL,
  `Name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `FK_Country_ID` int(10) UNSIGNED NOT NULL,
  `DateCreated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `DateUpdated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `State`
--

INSERT INTO `State` (`PK_ID`, `Name`, `FK_Country_ID`, `DateCreated`, `DateUpdated`) VALUES
(1, 'Odisha', 1, '2017-08-29 17:58:01', '2017-08-29 17:58:01'),
(2, 'California', 2, '2017-11-06 11:21:58', '2017-11-06 11:21:58');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Address`
--
ALTER TABLE `Address`
  ADD PRIMARY KEY (`PK_ID`),
  ADD KEY `FK_City_ID` (`FK_City_ID`),
  ADD KEY `FK_Employee_ID` (`FK_Employee_ID`);

--
-- Indexes for table `City`
--
ALTER TABLE `City`
  ADD PRIMARY KEY (`PK_ID`),
  ADD UNIQUE KEY `Name` (`Name`),
  ADD KEY `FK_State_ID` (`FK_State_ID`);

--
-- Indexes for table `Country`
--
ALTER TABLE `Country`
  ADD PRIMARY KEY (`PK_ID`),
  ADD UNIQUE KEY `Name` (`Name`);

--
-- Indexes for table `Employee`
--
ALTER TABLE `Employee`
  ADD PRIMARY KEY (`PK_ID`),
  ADD UNIQUE KEY `Email` (`Email`),
  ADD KEY `FK_Organization_ID` (`FK_Organization_ID`),
  ADD KEY `FK_Role_ID` (`FK_Role_ID`);

--
-- Indexes for table `Organization`
--
ALTER TABLE `Organization`
  ADD PRIMARY KEY (`PK_ID`),
  ADD UNIQUE KEY `Name` (`Name`);

--
-- Indexes for table `OrganizationRole`
--
ALTER TABLE `OrganizationRole`
  ADD PRIMARY KEY (`PK_ID`),
  ADD UNIQUE KEY `Unique_1` (`FK_Organization_ID`,`FK_Role_ID`),
  ADD KEY `FK_Organization_ID` (`FK_Organization_ID`),
  ADD KEY `FK_Role_ID` (`FK_Role_ID`);

--
-- Indexes for table `Role`
--
ALTER TABLE `Role`
  ADD PRIMARY KEY (`PK_ID`),
  ADD UNIQUE KEY `Name` (`Name`);

--
-- Indexes for table `State`
--
ALTER TABLE `State`
  ADD PRIMARY KEY (`PK_ID`),
  ADD UNIQUE KEY `Name` (`Name`),
  ADD KEY `FK_Country_ID` (`FK_Country_ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `Address`
--
ALTER TABLE `Address`
  MODIFY `PK_ID` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `City`
--
ALTER TABLE `City`
  MODIFY `PK_ID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `Country`
--
ALTER TABLE `Country`
  MODIFY `PK_ID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `Employee`
--
ALTER TABLE `Employee`
  MODIFY `PK_ID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;
--
-- AUTO_INCREMENT for table `Organization`
--
ALTER TABLE `Organization`
  MODIFY `PK_ID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `OrganizationRole`
--
ALTER TABLE `OrganizationRole`
  MODIFY `PK_ID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `Role`
--
ALTER TABLE `Role`
  MODIFY `PK_ID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `State`
--
ALTER TABLE `State`
  MODIFY `PK_ID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `Address`
--
ALTER TABLE `Address`
  ADD CONSTRAINT `FK_City` FOREIGN KEY (`FK_City_ID`) REFERENCES `City` (`PK_ID`),
  ADD CONSTRAINT `FK_Employee` FOREIGN KEY (`FK_Employee_ID`) REFERENCES `Employee` (`PK_ID`);

--
-- Constraints for table `City`
--
ALTER TABLE `City`
  ADD CONSTRAINT `FK_City_State` FOREIGN KEY (`FK_State_ID`) REFERENCES `State` (`PK_ID`);

--
-- Constraints for table `Employee`
--
ALTER TABLE `Employee`
  ADD CONSTRAINT `FK_Employee_Organization` FOREIGN KEY (`FK_Organization_ID`) REFERENCES `Organization` (`PK_ID`);

--
-- Constraints for table `OrganizationRole`
--
ALTER TABLE `OrganizationRole`
  ADD CONSTRAINT `OrganizationRole_ibfk_1` FOREIGN KEY (`FK_Organization_ID`) REFERENCES `Organization` (`PK_ID`),
  ADD CONSTRAINT `OrganizationRole_ibfk_2` FOREIGN KEY (`FK_Role_ID`) REFERENCES `Role` (`PK_ID`);

--
-- Constraints for table `State`
--
ALTER TABLE `State`
  ADD CONSTRAINT `FK_State_Country` FOREIGN KEY (`FK_Country_ID`) REFERENCES `Country` (`PK_ID`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
