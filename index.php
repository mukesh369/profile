<?php
/* Index ( Log in ) Page 
   Author : Mukesh Srivastav 
   */
   
if (isset($_COOKIE['userid'])) {
    // if Remember_Me Cookie is set, Decrypt it and redirect to profile page
        
    // Include the Security.php file to use decryption function and decrypt the file.
    require "Utilities/Security.php" ;
    $security=new Security();
    
    // Decrypt the cookie and redirect to profile home page.
    $_SESSION["userid"]=$security->hash('decrypt', $_COOKIE['userid']);
    header("Location:Profile.php");
} else if (isset($_SESSION["userid"])) {

    // if session is set redirect to profile.php
    header("Location:Profile.php");
} else {

    // else show the log in form
    include "Header.php";

    session_start();
    $_SESSION['csrf_token_signIn'] = md5(uniqid(rand(), true));
     
    include "indexHtml.php";
} ?>