<?php
// This page destroys the session,unset the cookie and display a logout notice for 3 seconds before redirecting to index.php
session_start();
require "Utilities/Validation.php";
if (isset($_SESSION['userid'])) {
    session_destroy() ;
    setcookie('userid', "", time() - 3600);
    Validation::display("You are logging off. Bbye.");
} else {
	Validation::display("You are already logged out!");
}
header("refresh:3;url=index.php") ;
