<?php
//The page handles request for edit address and contacts and submits the data to server and returns the response to client
session_start() ;


if (isset($_POST["Submit"]) && isset($_SESSION["userid"]) && $_POST['csrf'] == $_SESSION['csrf_token_editAddress']) {
    if ($_POST["phone"]!="" && $_POST["city"]!="" && $_POST["state"]!="") {
        require "Config/Database.php" ;
        require "Utilities/Validation.php";
        $database=new Database();
        
        $db=$database->getConnection();

        require "Objects/Employee.php";
        
        $emp=new Employee($db);
        
        $validation=new Validation();
        $obj = array("phone" => $_POST["phone"] , "city" => $_POST['city'], "state" => $_POST['state'], "fax" => $_POST["fax"], "addressType" => $_POST["addressType"], "street" => $_POST["street"], "zip" => $_POST["zipCode"]);
        $emp = $validation->validate($obj, $emp);
        
        $emp->recordId=$_SESSION["userid"] ;
        
    
        $f = $emp->updateAddress() ;
        if ($f) {
            $recordId=$emp->recordId;
            Validation::display("Address Updated");
            header("refresh:4,url=Profile.php");
        } elseif (!$f) {
            Validation::display("Some Database mismatch.Please try again");
            header("refresh:4,url=Profile.php");
        } else {
            Validation::display($f);
        }
    } else {
        header("Location:EditProfile.php?alert='Please fill all the details first.'");
    }
} else {
    header("Location:EditProfile.php?alert='Please submit the form first.'");
}
