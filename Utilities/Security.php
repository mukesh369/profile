<?php

/*Security class contains the methods for encryption and decryptions */
class Security
{
    //define("ENCRYPTION_KEY", "!@#$%^&*");

    /**
     * Returns an encrypted & utf8-encoded
     */

    public function encrypt($string)
    {
        require 'Config/Parameters.php';
        return base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key), $string, MCRYPT_MODE_CBC, md5(md5(ENCRYPTION_KEY))));
    }


    /**
     * Returns the decrypted value of given string
     */

    public function decrypt($encrypted)
    {
        require 'Config/Parameters.php';
        return rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5($key), base64_decode($encrypted), MCRYPT_MODE_CBC, md5(md5(ENCRYPTION_KEY))), "\0");
    }

    /**
     * alternative simple method to encrypt or decrypt a plain text string
     * initialization vector(IV) has to be the same when encrypting and decrypting
     *
     * @param string $action: can be 'encrypt' or 'decrypt'
     * @param string $string: string to encrypt or decrypt
     *
     * @return string
     */
    public function hash($action, $string)
    {
        $output = false;
        $encrypt_method = "AES-256-CBC";
        $secret_key = '!@#$%';
        $secret_iv = 'QWE@!@#';
        // hash
        $key = hash('sha256', $secret_key);
        
        // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
        $iv = substr(hash('sha256', $secret_iv), 0, 16);
        if ('encrypt' == $action) {
            $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
            $output = base64_encode($output);

        } elseif ($action == 'decrypt') {
            $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);

        }

        return $output;
    }
}
