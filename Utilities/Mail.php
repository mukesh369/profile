<?php
class Mail
{
    /*
     * Sends mail to the user
     * params receiver's address , receiver's name, subject , message of mail
     * returns true/ error message if mail sent or not
     */
    public function sendMail($to, $name, $subject, $body)
    {
        // PHPMailer library
        require "/var/www/html/profileapp/vendor/phpmailer/phpmailer/PHPMailerAutoload.php";

        require "Config/Parameters.php";
        $mail = new PHPMailer;

        // Enable verbose debug output
        $mail->SMTPDebug = 0;

        // Set mailer to use SMTP
        $mail->isSMTP();

        // Specify main and backup SMTP servers
        $mail->Host = 'smtp.gmail.com';
        
        // Enable SMTP authentication
        $mail->SMTPAuth = true;
        
        $mail->Username = MAIL_USER;
        $mail->Password = MAIL_PASSWORD;
        
        // Enable TLS encryption, `ssl` also accepted
        $mail->SMTPSecure = 'tls';

        // TCP port to connect to
        $mail->Port = 587;

        $mail->setFrom(MAIL_USER, MAIL_USERNAME);

        // Add a recipient
        $mail->addAddress($to, $name);
        $mail->addReplyTo(MAIL_USER, 'Feedback');
        $mail->isHTML(true);


        $mail->Subject = $subject;
        $mail->Body    = $body ;

        if (!$mail->send()) {
            $e= 'Mailer Error: ' . $mail->ErrorInfo;
            
            return "Sorry! Mail Configuration are not set up. Mail could not be sent." ;
        } else {
            return true ;
        }
    }
}
