<?php
/* This class is contains the method for testing input to prevent xss attack, trims the whitespaces */
class Validation
{
	/*
	 * checks if input is set then trims and add special chars for special characters for preventing xss attacks or return blank
     * param1 Data input to be checked
     * return validated data or blank. 
	 */

    public function testInput($data)
    {
        if (isset($data)) {
            $data = trim($data);
            $data = stripslashes($data);
            $data = htmlspecialchars($data);
            return $data;
        } else {
            return '';
        }
    }

    /*
     * Validating inputs and initializing into object/array
     * Params1 Obj Associative array containing properties of object/array and inputs to validate
     * Params2 Emp class Object/Array
     * Return Emp class object/array initialized with validated input 
     */
    public function validate($obj, $emp)
    {
        if (is_array($emp)) {
            foreach ($obj as $key => $value) {
                $emp[$key] = $this->testInput($value);
            }
        } else {
            foreach ($obj as $key => $value) {
                $emp->$key = $this->testInput($value);
            }
        }
        return $emp;
    }

    /*
     * @param Takes a string display it to top center. 
     */
    static public function display($msg)
    {
        echo "<h2 class='alert alert-info'><center>".$msg."</center></h2>";
    }
}
