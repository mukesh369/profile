<?php
class Database
{
 
    // specify your own database credentials in Parameters.php
    
    public $conn;
 
    /*
     * To get database connection
     * returns database connection object
     */
    public function getConnection()
    {
        require 'Parameters.php';
        $this->conn = null;
 
        try {
            
            $this->conn = new PDO("mysql:host=" . DB_HOST . ";dbname=" . DB_NAME, DB_USERNAME, DB_PASSWORD);

        } catch (PDOException $exception) {
            echo "Connection error: " . $exception->getMessage();
        }
 
        return $this->conn;
    }
}
