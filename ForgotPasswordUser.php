<?php
// This page accepts the email from the forgotPassword.php and sends the change password link to mail if mail id is found in users table.
session_start();
if (isset($_POST["Submit"]) && $_POST['csrf']==$_SESSION['csrf_token_forgotPsw']) {
    if (!empty($_POST["email"])) {
        require "Config/Database.php";
        $database = new Database();
        $db = $database->getConnection();

        require "Objects/Employee.php";
        $emp = new Employee($db);
        
        $flag = $emp->getId($_POST["email"]);
        if ($flag) {
            require "Utilities/Mail.php";
            require "Utilities/Security.php";
            require 'Utilities/Validation.php';

            $security = new Security();
            $encryptedId = $security->hash('encrypt', $emp->id);
            $mail = new Mail();

            // setting the mail parameters.
            $name = $emp->firstName." ".$emp->lastName;
            $subject = "Change password at Profile" ;
            $body = "Hi $name , <b><a href='mukesh.s/ChangePassword.php?id=".$encryptedId."' target='_blank'>Click here </a></b>to set the new password.";

            $response = $mail->sendMail($emp->email, $name, $subject, $body);
            
            if ($response) {
                $msg = "Please Check the mail for password update at ".$_POST["email"];
                Validation::display($msg);
            } else {
                Validation::display($response);
            }
        } else {
            Validation::display("No record found with the mail : ".$_POST["email"]);
            header("refresh:4;url='ForgotPassword.php'");
        }
    } else {
        header("Location:ForgotPassword.php?alert='Please fill the details first.'");
    }
} else {
    header("Location:ForgotPassword.php?alert='Please submit the details first.'");
}
