<?php

// This page handles the data from Signup.php and submits the data to server and sends the response back to client
session_start() ;
require 'Header.php';

if (isset($_POST["Submit"]) && $_POST['csrf'] == $_SESSION['csrf_token_signUp']) {
    
    // Check all the Required input to be not empty.
    if (!empty($_POST["fname"] . $_POST["prefix"] . $_POST["lname"] . $_POST["email"] . $_POST["psw"] . $_POST["conPsw"] . $_POST["organization"] . $_POST["role"] . $_POST["phone"] . $_POST["city"] . $_POST["state"] . $_POST["country"])) {

        // Server Side validation of password and confirmPassword.
        $regex= "^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})";
        $f1 = preg_match($regex, $_POST["psw"]);
        $f2 = preg_match($regex, $_POST["conPsw"]);

        // Password and confirmPassword matching on server side.
        if ($f1 && $f2 && $_POST["psw"] != $_POST["conPsw"]) {
            header("Location:SignUp.php?alert='Password and Confirm password does not match on server.'");
        }
        
        require "Config/Database.php" ;
        require "Utilities/Validation.php";
        $database = new Database();
        $db = $database->getConnection();
        
        require "Objects/Employee.php";
        $emp = new Employee($db);
        
        $validation = new Validation();
        
        // Inputs validation.
        $obj = array("firstName" => $_POST["email"], "middleName" => $_POST['psw'], "lastName" => $_POST["lname"], "email" => $_POST["email"], "password" => $_POST["psw"], "prefix" => $_POST["prefix"], "organization"=>$_POST["organization"], "role"=>$_POST["role"], "phone" => $_POST["phone"], "city" => $_POST["city"], "state" => $_POST["state"], "country" => $_POST["country"], "DOB" => $_POST["DOB"], "maritalStatus" => $_POST["maritalStatus"], "gender"=>$_POST["gender"], "fax"=>$_POST["fax"], "addressType"=>$_POST["addressType"], "street"=>$_POST["street"], "zip"=>$_POST["zipCode"], "extraNote"=>$_POST["extraNote"]);
        $emp = $validation->validate($obj, $emp);
        
        /* Perform sign Up operation
         * if true then send mail for activate the account
         * else show the custom error message
         */
        $flag = $emp->signUp() ;
        

        if ( $flag ) {
            $recordId=$emp->recordId;

            require "Utilities/Security.php";

            $security = new Security();
            // before sending the id to activate , encrypt it.
            $encryptedId = $security->hash('encrypt', $recordId);

            require "Utilities/Mail.php";

            $mail = new Mail();

            //setting details fpr the mail to be sent.
            $name = $emp->firstName." ".$emp->lastName;
            $subject = 'Welcome to Profile';
            $body = 'Dear '.$emp->firstName.' , <br/> Welcome to Profile community. Please activate your account from the given link. <a  href="mukesh.s/AccountActivation.php?id='.$encryptedId.'"><b>Activate account from here.</b></a> <br/> Thanks for Signing up. <br/> Team Connect.';

            // send mail to activate the account on user's email address.
            $response = $mail->sendMail($emp->email, $name, $subject, $body);

            if ( $response ) {
                $msg = "Signed up successfully. Please Check the mail ".$emp->email." to activate your account.";
                
            } else {
                $msg = "Signed up but there is some mail issue. You can log in now. ".$response ; 
            }
            Validation::display($msg);
        } else if ( !$flag ) {
            $msg = "Account already exists with the email. Please check the details and sign up again";
            Validation::display($msg);
            header("refresh:4,url=SignUp.php");
        } else {
            Validation::display($flag);
        }
    } else {
        // if any required input is blank redirect to fill it back.
        header("Location:SignUp.php?alert='Please fill all the details first.'");
    }
} else {
    // if form is not submitted or csrf is not set, redirect it back.
    header("Location:SignUsp.php?alert='Please submit the form first.'");
}
