# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)


This Repository contains a profile project that includes basic web features i.e. Sign In , Sign Up , Sign Out , Forgot Password, Remember me, Verification using SMTP mail , encryption & decryption. It was my first project into web development and it's my first project on BitBucket too .

### Database/Mailing/Security Configurations : ###
This is mentioned in config/Parameters.php file. Please set that up before running the project.

### Folder Structure : ###

1. Objects folder contains Employee.php which contains main functionalities of the project i.e. signIn, signUp, updateData, fetchUserInformation and many more as well all the user's properties.

2. Utility folder contains helper classes i.e. Security class for Encryption and decryption, Validation class for server side validation, Mail class for sending the mail etc. 

3. Config folder contains Database connectivity and Parameters file. 

4. Migration folder contains Sql file for the database.  Import that file before running the project.

5. Static folder contains Css and Js files. Js files includes Plain javascript validation for password and password matching, ImgUpload via Ajax, Jquery code for navigation highlighting.

6. Files in root folder contains view and control part of MVC architecture. Model, basically business logic, sql queries resides in Objects/Employees.php . 

7. Documentation folder contains PSR2 coding guideline document in pdf format. 
 

### Coding Guideline Document - ###
This project follows the PSR2 coding standard which is described [here](https://github.com/php-fig/fig-standards/blob/master/accepted/PSR-2-coding-style-guide.md) 

### Coding Convention - ###
PSR2 is followed throughout the project.

### Quality Check - ###
SonarQube Quality gate passed. 

### Functionalities - ###
This is a sample profile project what it does that - 

1. On sign up page, It takes the inputs user's personal, professional, address and contacts information.

2. It validated that password field must be minimum 8 length having number, lowercase , uppercase , special characters and it should match with confirm password before submitting to the server.

3. On server side sign up, It checks again for confirm password and password matching if not same , then it redirects you to signup page again with a message of "Password and Confirm password matching failed at server end.".

4. At signup server end, before saving the password into database , It converts the password as hash using one way password_hash() function with Bcrypt algorithm.

5. If all the data (firstName , middleName, lastName, email, password, designation, organization, address, state, country, zip, city) got succesfully inserted then we get its recordId in database and store it in the object's recordId field and send the account activation mail on the given user's email , name (firstName + lastName). In mail's body there we set a link to account activate page with a url parameter id which contains the object's record id in encrypted form (openssl_encrypt()) .

6. At account activation page , It expects the id parameter from url and decrypt it using security object encrypt_decrypt('decrypt' , $id) method and activats the account. In case id not found it shows message according to related case.

7. Now you can sign in and go to user's dashboard which is quite simple now. It will show only user's name and email and logout button. I will add more details and functionalities here like profile picture setting, building professional profile, updating information, searching and exploring other user's profile. 

8. This project exploits object oriented programming in php. We have created a employee class with all its fields (name, email , photo, gender, role, organization, marital status, DOB) and functions which employee performs in the project as sign in, sign up, updateInfo , getInfo etc. 

9. We have also created many other utilities classes like mail , security, validation which in utilities folder. 

10. I have put database connection file in config folder , employee class file in objects folder. And included each (require) as needed.  

11. In signup page after putting the details, it checks if the details matched with any record in the database using two step verification that means it first checks if email exists or not , if yes then it extracts record's id and password. Email is an unique key in table so it can't be repeated. After that it checks if password is matched or not using password_verify () function as extracted password is in  one way hashed form. If matched then it stores record's id into the session variable and also if remember me is checked then it also store it in cookie and redirects to profile page after showing successfully authenticated message. 

12. On signout , it simply destroy the session and unset the cookie and shows message for log out. 

13. Do not forget to change the  database and mail parameters in Config/Parameters.php file before use.

This Repository contains a profile project that includes basic web features i.e. Sign In , Sign Up , Sign Out , Forgot Password . It was my first project into web development and it's my first project on BitBucket too . 



### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

How to run the project :

1. Create a folder named in your apache localhost folder and simple put all these files in there.

2. It uses a database named as "ProfileApp". To set up a database to run the project please import the file "ProfileApp.sql" located in migration folder in the project directory. Notify me if there is any issue comes.

3. Simply open the browser with url "localhost/<created_folder_name>" and hit enter.Whoa. Login window is opened.Simply start from a Signup because most likely this would be your first time at the site so you won't have a password right now.

4. After signup, you get a mail for activation, and now you are able to go to dashboard after login.


### Technology ###
PHP, PDO (PHP data object ), OOP concepts, JQuery, Bootstrap, CSS, HTML5, AJAX, JavaScript.

  
### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact
