<!-- Forgot password form accepts email and submits it forgotPasswordUser.php to handle--> 
<?php require "Header.php" ; ?>
<body>
<h2><center>Change Password</center></h2>
<div id="id01" class="container-fluid">
  
  
    <?php
      require "UserIcon.php" ;
      $_SESSION['csrf_token_forgotPsw'] = md5(uniqid(rand(), true));
    ?>
    <form class="form-horizontal" id="forgotPasswordForm" method="POST" action="ForgotPasswordUser.php">
      <fieldset>
      <input type="hidden" name="csrf" value="<?php echo $_SESSION['csrf_token_forgotPsw'];  ?>">
      <div class="form-group">
        <label class="control-label col-sm-3" for="email">Email :</label>
        <div class="col-sm-9">
          <input type="email" class="form-control" id="email" name="email" placeholder="Enter email" required>
        </div>
      </div>
      <div class="form-group"> 
        <div class="col-sm-offset-2 col-sm-10">
          <button type="submit" name="Submit" class="btn btn-primary btn-block">Submit</button>
        </div>
      </div>
      <div class="form-group sidelinks">
      <a href="index.php">Sign In here!</a>
      <span class="h5">Sign up <a href="SignUp.php">here !</a></span>
      </div>
      </fieldset>
    </form>
</div>
</body>
</html>