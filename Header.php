<!-- Header page with various stylesheets and scripts links, an alert script to show the messages -->
<!DOCTYPE html>
<html>
<head>
  <title>ProfileApp</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="Static/Css/Style.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script src="Static/Js/Script.js"></script>
  <script>
  <?php if (isset($_GET["alert"])) { ?>
     alert(<?php echo $_GET["alert"]; ?>) ; 
  <?php }?>
  </script>
</head>