<?php 
    session_start();

    $_SESSION['csrf_token_changeDp'] = md5(uniqid(rand(), true));
?>

<nav class="navbar navbar-inverse">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>                        
          </button>
          <a class="navbar-brand" href="Profile.php"><span class="glyphicon glyphicon-user"></span> Profile</a>
        </div>
        <!-- Navbar for navigation -->
        <div class="collapse navbar-collapse" id="myNavbar">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="Profile.php">View Profile</a></li>
            <li><a href="EditProfile.php">Edit Profile</a></li>
            <li><a href="#" data-toggle="modal" data-target="#changeprofilepic"><span class="glyphicon glyphicon-picture"></span>Change Profile Picture</a></li>
            <li><a href="LogOut.php"><span class="glyphicon glyphicon-log-out"></span>Log-Off</a></li>
          </ul>
        </div>
      </div>
    </nav>

    <!-- Open this Modal on click on change profile picture navigation link-->
    <div id="changeprofilepic" class="modal fade" role="dialog">
      <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Select the picture :</h4>
          </div>
          <div class="modal-body">
            <p>It should be a JPG image file and less than 5 MB.</p>
            <form action="UploadProfilePictureAjax.php" id="uploadPicture" method="POST" class="form-inline" enctype="multipart/form-data">
              <div class="alert" id="message"></div>
              <input type="hidden" name="csrf" value="<?php echo $_SESSION["csrf_token_changeDp"];?>">
              <div class="form-group">
                <input type="file" class="" name="profilePhoto" id="profilePhoto">
              </div>
              <button type="submit" class="btn btn-sm btn-primary" name="Submit">Upload</button>
              </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>

      </div>
    </div>

<script type="text/javascript" src="Static/Js/ImgUploadViaAjax.js"></script>