<?php 

// This page handles the request coming from index.php and queries the response and sends it back

session_start() ;

require "Utilities/Validation.php";
require "Config/Database.php";
require "Objects/Employee.php";
require "Utilities/Security.php";
if (isset($_POST["Submit"]) && $_POST["csrf_token"]==$_SESSION['csrf_token_signIn']) {
    if ($_POST["email"]!='' && $_POST["psw"]!='') {
        $dataBase=new Database();
        $db=$dataBase->getConnection();

        // Main class in file Employee.php with all functionalities.
        $emp=new Employee($db);

        $validate=new Validation();
        
        $obj = array("email" => $_POST["email"] , "password" => $_POST['psw']);
        $emp = $validate->validate($obj, $emp);

        // Perform Sign in operation via Sign in method.

        $message = $emp->signIn();

        if ($message) {
            $_SESSION['userid']=$emp->id;
            Validation::display(" Successfully Authenticated.");

            // if remember me is set , set user id in encrypted format as cookie.
            if (isset($_POST['remember_me'])) {
                $security=new Security();
                $enc=$security->hash('encrypt', $emp->id);
                setcookie('userid', $enc, time() + (7200));
            }

            header("refresh:3;url=Profile.php") ;
        } elseif (!$message) {
            $msg= "Wrong Email/Password. Please check and try again";
            Validation::display($msg);
            header("refresh:3;url=index.php") ;
        } else {

            //Show custom message on exception.
            Validation::display($message);
        }
    } else {

        // if email or password is not set redirect to index.php with message.
        header("Location:index.php?alert='Please fill all the details first.'");
    }
} else {

    // if csrf is not set or submit is not clicked redirect to index.php.
    header("Location:index.php?alert='Please submit the details first.'");
}
