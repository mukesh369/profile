<?php
// This page shows the client view for edit profile
session_start();
if (isset($_SESSION["userid"])) {
    include "Objects/Employee.php";
    include "Config/Database.php";
    $database = new Database();
    $db = $database->getConnection();
    $emp = new Employee($db);
    $msg = $emp->getProfileInfo($_SESSION["userid"]);
  
    $_SESSION['csrf_token_editPersonal'] = md5(uniqid(rand(), true));
    $_SESSION['csrf_token_editProfessional'] = md5(uniqid(rand(), true));
    $_SESSION['csrf_token_editAddress'] = md5(uniqid(rand(), true));

    if ($msg) {
        include "Header.php" ;

        include "EditProfileHtml.php" ;

    } else {
        echo "No data found with the given id" ;
    }
} else {
    header("Location: index.php");
} ?>








