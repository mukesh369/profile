<?php session_start() ;

// Change password form for setting password. This page appears after clicking the link in the forget password mail. 

  if (isset($_GET['id'])) {
      
      //Decrypt the userid
      require "Utilities/Security.php";
      $security=new Security();
      $id=$security->hash('decrypt', $_GET['id']);
      require "Header.php" ;

      $_SESSION['csrf_token_changePsw'] = md5(uniqid(rand(), true));

      ?>

<body>
<h2  ><center>Change Password</center></h2>
<div id="id01" class="container-fluid">
    <?php require "UserIcon.php"; ?>
    <form class="form-horizontal" id="changePasswordForm"  method="POST" action="ChangePasswordUser.php" onsubmit="return validateForm()">
      <fieldset>
      <!-- To show the alert message -->
      <div class="alert" id="message"></div>

      <!-- CSRF Token -->
      <input type="hidden" name="csrf" value="<?php echo $_SESSION['csrf_token_changePsw']; ?>">

      <!-- Userid -->
      <input type="hidden" name="id" value=<?php echo $id ; ?> >

      <!-- New password -->
      <div class="form-group">
        <label class="control-label col-sm-3" for="pwd">New Password :</label>
        <div class="col-sm-9"> 
          <input type="password" class="form-control" id="pwd" name="psw" placeholder="Enter new password" title="minimum 8 length having uppercase , lowercase , number , special characters." required>
        </div>
      </div>

      <!-- Confirm password -->
      <div class="form-group">
        <label class="control-label col-sm-3" for="conpwd">Confirm New Password :</label>
        <div class="col-sm-9"> 
          <input type="password" class="form-control" id="conpwd" name="conPsw" placeholder="Confirm new password" title="must be same as Password" required>
        </div>
      </div>

      <!-- Submit button -->
      <div class="form-group"> 
        <div class="col-sm-offset-2 col-sm-10">
          <button type="submit" name="Submit" class="btn btn-primary btn-block">Submit</button>
        </div>
      </div>

      <!-- Links to go Sign in and Sign up page -->
      <div class="form-group sidelinks">
      <a class="" href="index.php">Sign In here!</a>
      <span class="h5">Sign up <a href="SignUp.php">here !</a></span>
      </div>
      </fieldset>
    </form>
</div>

</body>
</html>
<?php
  } else {
    // If request is not from the mail link means id is not set, forbidden access.
    require "Utilities/Validation.php";
    Validation::display("Forbidden Access");
} ?>